# SimpleTextWriterJS v1.0.0 #

A simple and nimble WYSWYG editor
although not powerful but does the job.

### Requirements ###
* DOMPurifer - sanitizing HTML contents. Check it out at https://github.com/cure53/DOMPurify.

### Setup ###
* Place all required libraries inside `lib` folder.
* Please see demo.html as an example for initiating the editor.
	
### Hooks ###
* *beforeExecuteAction* - triggers before a tool action is executed. Tool object is passed as parameter.
* *afterExecutionAction* - triggers after a tool action is executed. Tool object is passed as paremeter.
* *preSetContent* - triggers before setting editor content when calling .setContent()

### Things to add ###
* *CreateLink*
* *insertImage*
* *CSS*
* Word/Character counter

### Current Issues ###
* IE *insertOrderedList* and *insertUnorderedList* tends to all element if root does not have contenteditable set.

### Big Thanks To ###
TinyMCE for an idea on saving selection point with undo commands.
TextAngular for an idea on setting up tools for the editor.
