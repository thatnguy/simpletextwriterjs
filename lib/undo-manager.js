;(function(root, document, factory, undefined) {
	'use strict';
	
	// use AMD if have support.
	if (typeof define === 'function' && define.amd) {

		// wrapping our factory for AMD loader.
		// and passing the return value to `root` (window).
		define(['lib/utils'], function() {
			return ( root.UndoManager = factory(utils) );
		});

	} else {
		// browser globals.
		root.UndoManager = factory(root.utils);
	}
})(this, document, function(utils) {

	// simple Undo Manager.
	return function() {

		var stack   	= [],
			index       = -1,
			limit       = 1000,		// default stack limit. *note* 0 = unlimited
			self        = this;

		var save = function(command) {

			// command must be an object.
			if (typeof command === 'undefined' || !utils.isObject(command)) {
				return self;
			}

			// must define an `undo` and `redo` function for the command.
			command = utils.extend({
				undo: function() { 
					throw new Error('No Undo function set for command.'); 
				},
				redo: function() {
					throw new Error('No Redo function set for command.');
				}
			}, command);

			// update the stack with current index.
			if (index < stack.length - 1) {
				stack = stack.slice(0, index + 1);
			}

			stack.push(command);

			// move oldest command from stack when reached limit.
			if (!utils.isEmpty(limit) && stack.length > limit) {
				stack.shift();
			}

			// update index to end of stack.
			index = stack.length - 1;

			return self;

		};

		var _execute = function(command, action) {
			if (!utils.isFunction(command[action])) {
				return self;
			}

			var result = command[action].call(command);

			return result;
		}

		var undo = function() {
			if (!canUndo()) {
				return self;
			}

			var command = stack[index--];

			return _execute(command, 'undo');

		};

		var redo = function() {
			if (!canRedo()) {
				return self;
			}

			var command = stack[++index];
			
			return _execute(command, 'redo');

		};

		var replace = function(index, command) {
			var _command = stack[index];

			if (_command) {
				stack[index] = command; 
			}

			return self;
		}

		var replace = function(cmd, idx) {
			idx = idx || index;

			if (idx < 0) {
				idx = stack.length - 1;
			}


			return stack[idx] ? (stack[idx] = cmd) : self;
		}

		var current = function() {
			return stack[index];
		};

		var setLimit = function(newLimit) {
			if (typeof newLimit !== 'number') {
				return false;
			}

			limit = newLimit;
		}

		var reset = function() {
			stack = [];
			index = -1;
		}

		var getCommands = function() {
			return stack;
		}

		var canUndo = function() {
			return index > -1;
		}

		var canRedo = function() {
			return (stack.length - 1) > index;
		}

		// @remove
		var getPosition = function() {
			return index;
		}

		 // expose public functions
		return {
			save: save,
			undo: undo,
			redo: redo,
			canUndo: canUndo,
			canRedo: canRedo,
			reset: reset,
			setLimit: setLimit,
			current: current,
			replace: replace,
			getCommands: getCommands,
			getPosition: getPosition
		};
	}

});