;(function(root, document, factory, undefined) {
	'use strict';
	
	// use AMD if have support.
	if (typeof define === 'function' && define.amd) {

		// wrapping our factory for AMD loader.
		// and passing the return value to `root` (window).
		define(function() {
			return (root.utils = factory());
		});

	} else {
		// browser globals.
		root.utils = factory();
	}
})(this, document, function() {

	var objectProto = Object.prototype;

	var hasOwnProp = objectProto.hasOwnProperty;

	var objectCtorString = hasOwnProp.toString.call(Object);

	var _entities = {
		'&' : '&amp;',
		'<' : '&lt;',
		'>' : '&gt;',
		'"' : '&quot;',
		"'" : '&#39;',
		'/' : '&#x2F;'
	};

	// helper functions here.
	return {

		// STRINGS

		// escape html entities for strings.
		escapeHTML : function(str) {
			var pattern = /[&<>'"\/]/g;

			return str.replace(pattern, function(match) {
				return _entities[match];
			});
		},

		// trim white spaces from beginning and end of `str`.
		trim : function(str) {
			return this.isEmpty(str) ? '' : str.replace(/^[\s\uFEFF\xA0&nbsp;]+|[\s\uFEFF\xA0&nbsp;]+$/g, '');
		},

		diff : function(strA, strB) {

			// strA will always be the longer string.
			if (strA.length < strB.length) {
				var tmp = strA;
				strA = strB;
				strB = tmp;
			}

			var matchedIdx = strA.indexOf(strB), diff = [];

			// no match
			if (matchedIdx == -1) {
				return diff;
			}

			// matched at start.
			if (matchedIdx == 0) {	// matched at start
				diff.push( strA.substr(strB.length) );
			} else {
				diff.push( strA.substr(0, matchedIdx) );

				// matched in between.
				if ((matchedIdx + strB.length) < strA.length) {
					diff.push( strA.substr(matchedIdx + strB.length));
				}
			}

			return diff;
		},

		// OBJECTS

		// if object has property `key`
		has : function(obj, key) {
			return hasOwnProp.call(obj, key);
		},

		// check if `obj` is an object.
		isObject : function(obj, isPlain) {
			return obj != null && (typeof obj === 'object' || typeof obj === 'function');
		},

		// check if object is plain, such as those created with Object.create(null), Object() constructor
		isPlainObject : function(obj) {

			var objString = objectProto.toString.call(obj), proto, Ctor;

			if (obj == null || !obj || objString !== '[object Object]') {
				return false;
			}

			proto = Object.getPrototypeOf(obj);

			// object created with Object.create(null) are plain.
			if (!proto || proto === null) {
				return true;
			}

			var Ctor = hasOwnProp.call(proto, 'constructor') && proto.constructor;

			return typeof Ctor === 'function' && Ctor instanceof Ctor &&
				hasOwnProp.toString.call(Ctor) === objectCtorString;
		},

		isArray : function(obj) {

			// when Array.isArray is defined we use that instead.
			if (Array.isArray) {
				return Array.isArray(obj);
			}

			return objectProto.toString.call(obj) === '[object Array]';
		},

		isFunction : function(obj) {

			// if (!this.isObject(obj, false)) {
			// 	return false;
			// }
			if (!this.isObject(obj)) {
				return false;
			}

			obj = Object(obj);

			var tag = objectProto.toString.call(obj);

			return tag === '[object Function]' 			||
			   	   tag === '[object GeneratorFunction]' ||
			   	   tag === '[object AsyncFunction]' 	||
			   	   tag === '[object Proxy]';
		},

		isElement : function(obj) {
			return !this.isPlainObject(obj) && obj.nodeType === 1;
		},

		// credits to Sean Vieira @ http://stackoverflow.com/questions/4994201/is-object-empty
		isEmpty : function(obj) {

			if (obj == null) {
				return true;
			}

			// just a quick check though not very `secure` as length property can be feed to object with value > 0.
			if (obj.length && obj.length > 0) {
				return false;
			}

			if (typeof obj === 'number' && obj > 0) {
				return false;
			}

			// if (this.isObject(obj)) {
			// 	return true;
			// }
			if (this.isPlainObject(obj)) {
				return true;
			}

			// does object have its own properties?
			for (var prop in obj) {
				if (hasOwnProp.call(obj, prop)) {
					return false;
				}
			}

			return true;
		},

		getKeys : function(obj) {
	 		var keys = [];

	 		// if (!this.isObject(obj)) {
	 		// 	return keys;
	 		// }
	 		if (!this.isPlainObject(obj)) {
	 			return keys;
	 		}

	 		for (var key in obj) {
	 			keys.push(key);
	 		}

	 		return keys;
	 	},

	 	// Deep clone an object
	 	clone : function(obj) {

	 		// if (!this.isObject(obj) || this.isFunction(obj)) {
	 		// 	return obj;
	 		// }

	 		if (!this.isPlainObject(obj) || this.isFunction(obj)) {
	 			return obj;
	 		}

	 		if (this.isArray(obj)) {
	 			return obj.slice();
	 		}

	 		// plain object cloning

	 		var cloneObj = {};

	 		for (var key in obj) {
				cloneObj[key] = this.clone(obj[key]);
	 		}

	 		return cloneObj;
	 	},


	 	// shallow copy of an object
	 	copy: function(obj) {
	 		var o = {}, key;

	 		for (key in obj) {
	 			o[key] = obj[key];
	 		}

	 		return o;
	 	},

	 	extend : function(target, source) {

			var keys = this.getKeys(source);

			for (var i = 0, l = keys.length; i < l; i++) {
				var key = keys[i], prop = source[key];

				// check if prop is an object
				if (this.isObject(prop)) {
					prop = this.clone(prop);
				}

				target[key] = prop;
			}

			return target;
		},


		getSelection : function() {

			var selection;

			if (window.getSelection || document.getSelection) {
				selection = window.getSelection() || document.getSelection();
			} else {
				selection = document.selection.createRange();
			}

			return selection.rangeCount > 0 ? selection : false;
		},


		// move caret to end of an element
		moveCaretToEnd : function(el) {

			if (el.nodeType === Node.ELEMENT_NODE) {
				el.focus();
			}

			var range = document.createRange();
			range.selectNodeContents(el);

			// move start offset to end
			range.collapse(false);

			var selection = this.getSelection();

			// reset the ranges in selection and add our own range.
			selection.removeAllRanges();
			selection.addRange(range);

		},


		// credits to gordonbrander @see https://gist.github.com/gordonbrander/2230317
		generateID : function(prefix) {
			prefix = typeof prefix !== 'undefined' ? prefix : '_';

			return prefix + Math.random().toString(36).substr(2);
		},


		removeElement : function(el, removeChildren) {
	 		removeChildren = typeof removeChildren !== 'undefined' ? removeChildren : true;

	 		var parent = el.parentNode;

	 		if (!removeChildren) {
	 			// move children to new parent.
	 			while (el.firstChild) {
	 				parent.insertBefore(el.firstChild, el);
	 			}
	 		}

	 		// remove the element itself.
	 		return parent.removeChild(el);
	 	},

	 	replaceNode : function(node, nodes) {
			var parent = node.parentNode;

			for (var i = 0, l = nodes.length; i < l; i++) {
				parent.insertBefore(nodes[i], node);
			}

			parent.removeChild(node);

			return parent;
	 	},

	 	getTextNode : function(node, boundary, reverse) {
			reverse = typeof reverse !== 'undefined' ? reverse : false;
			
			var child = reverse ? 'lastChild' : 'firstChild',
				sibling = reverse ? 'previousSibling' : 'nextSibling';

			while (node) {

				if (node.nodeType === Element.TEXT_NODE) {
					return node;
				}

				if (node == boundary) {
					return false;
				}

				// children
				if (node[child]) {
					node = node[child];
					continue;
				}

				// siblings.
				if (node[sibling]) {
					node = node[sibling];
					continue;
				}

				// go to parent.
				// we must find a new parent and not the one we are under.
				if (node.parentNode) {
					while (node = node.parentNode) {
						if (node[sibling]) {
							node = node[sibling];
							break;
						}
					}
				}
			}

			return node;
		},

	 	// merge two arrays. does not merge duplicates.
	 	// @todo paramater flag for `unique` can be set which should trigger the unique() on target
	 	merge : function(target, source, unique) {
	 		unique = !!unique;

	 		if (!utils.isArray(target) || !utils.isArray(source)) {
	 			return target;
	 		}

	 		var i = 0, l = source.length;

	 		for ( ; i < l; i++) {
	 			target.push( source[i] );
	 		}

	 		if (unique) {
	 			target = this.unique(target);
	 		}

	 		return target;
	 	},

	 	uniqueSort : function(array, comparator) {

	 		var result = [];

	 		if (!this.isArray(array)) {
	 			return result;
	 		}

	 		// sort the array
	 		array.sort(comparator);

	 		return this.unique(array);
	 	},

	 	unique : function(array) {
	 		var result = [], i = -1, l = array.length;
	 		
	 		out:
	 		while (++i < l) {
	 			var val = array[i],
	 				  j = result.length;

	 			while (j--) {
	 				if (result[j] === val) {
	 					continue out;
	 				}
	 			}

	 			result.push(val);
	 		}

	 		return result;
	 	}

	}
});
