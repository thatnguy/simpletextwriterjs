/*
 * SimpleTextWriterJS - A very simple WYSWYG text editor.
 * Copyright 2016 Phong Nguyen
 *
 * SimpleTextWriterJS can be freely distributed under the MIT license.
 */

;(function(root, document, factory, undefined) {
	
	'use strict';

	// use AMD if have support.
	if (typeof define === 'function' && define.amd) {

		// wrapping our factory for AMD loader.
		// and also exposing to root (window).
		define(
			['lib/event-dispatcher', 'lib/undo-manager', 'lib/utils', 'lib/dom-purifier'],
			function(eventDispatcher, UndoManager, utils, DOMPurifier) {
				return ( root.Editor = factory(eventDispatcher, UndoManager, utils, DOMPurifier) );
			}
		);

	} else {

		// browser globals.
		root.Editor = factory( root.eventDispatcher, root.UndoManager, root.utils, root.DOMPurifier );
	}

// `this` can refer to `window` in browser environment or another global object in a non-browser environment.
})(this, document, function(eventDispatcher, UndoManager, utils, DOMPurifier) {

	// constants

	var EDITOR_ID_PREFIX = 'editor_';

	var TOOL_PREFIX = 'tool-';

	var ALLOWED_TOOL_ELEM_TAGS = /^(button|div|p)$/i;

	var version = '1.0.0';

	// native command white list.
	var nativeCommands = [
		'bold', 'italic', 'underline', 'strikeThrough',
		'heading', 'insertParagraph', 'insertImage',
		'indent', 'outdent', 'insertOrderedList', 'insertUnorderedList',
		'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull',
		'createLink', 'unlink', 'contentReadOnly', 'fontSize', 'forwardDelete',
		'formatBlock'
	];

	var queryCommandState = document.queryCommandState.bind(document) || _queryCommandState;

	var queryCommandValue = document.queryCommandValue.bind(document) || _queryCommandValue;

 	var _isNativeCommand = function(cmd) {
		for (var i = 0, c, l = nativeCommands.length; i < l; i++) {
			c = nativeCommands[i];
			if (cmd === c) {
				return true;
			}
		}

		return false;
	};

	// current not implemented functions.
 	var _queryCommandState = function(command) {
 		throw new Error('This function is not supported.');
 	}
 	var _queryCommandValue = function(command) {
 		throw new Error('This function is not supported.');
 	}

 	// since there is an inconsistent result of `queryCommandValue` on `formatBlock`
 	// across browsers. We will just do a search for parent Node if matched `tag`.
 	// 
 	// current incompatibility with queryCommandValue:
 	// 	- in IE `blockquote` tag is returned as `Formatted`
 	// 	- in IE other tags besides H1-H6, P, ADDRESS, PRE will return `Normal`
 	// 	- in Firefox other tags besides H1-H6, P, ADDRESS, PRE will return empty.
 	var queryFormatBlockState = function(tag, boundary) {
 		var sel = utils.getSelection();

 		tag = tag.toUpperCase();
 		
 		if (!sel) {
 			return false;
 		}

 		var node = sel.anchorNode;

 		while (node) {
 			if (node.nodeType === 1 && node.nodeName === tag) {
 				return true;
 			}

 			if (boundary && node == boundary) {
 				return false;
 			}

 			node = node.parentNode;
 		}

 		return false;
	 	
 		// return tag.toLowerCase() === queryCommandValue('formatBlock').toLowerCase();
 	}

 	// remove format by elemTag of current selection.
	var removeFormat = function(elemTag, boundaryElem, newParent) {
 		boundaryElem = typeof boundaryElem !== 'undefined' ? boundaryElem : false;

 		if (typeof newParent === 'undefined' || !newParent instanceof Element) {
 			newParent = document.createElement('DIV');
 		}

 		elemTag = elemTag.toUpperCase();

 		var sel = utils.getSelection();

 		if (!sel) {
 			return false;
 		}

 		var range = sel.getRangeAt(0), node = range.endContainer;

 		while (node) {
 			// matched elemTag
 			if (node.nodeType === Node.ELEMENT_NODE && node.nodeName === elemTag) {
 				break;
 			}

 			// when boundary element set and we reaching this point means failure.
 			if (boundaryElem && boundaryElem == node) {
 				return false;
 			}

 			if (!(node = node.parentNode)) {
 				return false;
 			}
 		}

 		// when removing the format block we need a container to hold the new children.
 		// this applies in the case when the format is not a first child.
 		// `newParent` will be the new container in this case.
 		var _focussedNode;
 		if (node.previousSibling) {

 			// move children to new parent.
 			while (node.firstChild) {
 				newParent.appendChild(node.firstChild);
 			}
 			node.parentNode.insertBefore(newParent, node);

 			utils.removeElement(node);

 			_focussedNode = newParent;
 		} else {
 			_focussedNode = node.lastChild || node.parentNode;
 			utils.removeElement(node, false);
 		}

 		utils.moveCaretToEnd(_focussedNode);
 	};

 	// add single or multiple native events.
 	var addEvents = function(elem, events, callback) {
 		if (!utils.isElement(elem)) {
 			throw new Error('addEvents: parameter 1 given is not of type Element.');
 		}

 		if (typeof events === 'string') {
 			if (events.length < 1) {
 				throw new Error('addEvents: parameter 2 given is empty.');
 			}

 			events = [events];
 		}

 		if (!utils.isArray(events)) {
 			throw new Error('invalid list of events');
 		}

 		for (var i = 0, l = events.length; i < l; i++) {
 			elem.addEventListener(events[i], callback);
 		}
 	}

 	/* 
		list of tools definitions to register:

		name: (string) name of the tool (must be unique to other tools)

		action: (function(tool)) function that triggers upon a click event on the tool. `this` context refers to Editor instance.

		tag?: (string) define the tools element tag e.g. 'button'

		text?: (string) define the text which will appear on the tool. If none specified then tool `name` will be used.

		class?: (string) extra classes that will be added to the tool. Please use string type. Multiple classes are denoted by spaces.

		tooltip?: (string) text which appears when hover on a tool.
		
		disabled?: (true|false) set availability of tool.

		disableState: (function($el) || boolean) function which is called every keystroke, click on editor to update the disable
												 state of the tool. This function MUST return a boolean which denotes whether to
												 disable the tool or not.

		activeState?: (function($el) || boolean) used to pass in an own function for updating the tool state
												 rather than using the default when toggling the tool.
												 When passing a function, the function must return a boolean
												 with `true` being active and `false` not active.
												 To not use ANY active state at all pass `false` to this property.


		shortcutKey?: (int character code) shortcut key when pressed with `ctrl` key to trigger the tool action.

		toggleUndo?: (true|false) set if this tool's action should be captured as an Undo level.

	*/
	var registerTools = [

		// format blocks.
		(function() {

	 		var tools = [];

	 		// resuable functions for our formatting block tools.
	 		var _action = function(tool) {
	 			var editor = this.getEditorElement();

	 			if (!tool._state) {
	 				// remove format
	 				removeFormat(tool.formatTag, editor, document.createElement('DIV'));

	 			} else {

	 				// NOTE `tool` needs to have `tag` property wih specified tag to format
	 				this.execCommand('formatBlock', false, tool.formatTag);
	 			}

	 			editor.normalize();
	 		};

	 		var _activeState = function(tagName, el) {
	 			var root = document.querySelector('.content-text');

	 			return queryFormatBlockState(tagName, root);
	 		};

	 		// we use `formatBlock` command for headings because
	 		// `heading` command is not supported in IE or Safari.
	 		for (var i = 0, headings = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'], l = headings.length; i < l; i++) {
	 			var name = headings[i];

	 			var obj = {
	 				name: name,
	 				text: name.toUpperCase(),
	 				action: _action,
	 				tooltip: 'Heading ' + name.charAt(1),
	 				activeState: _activeState.bind(this, name),
	 				formatTag: name
	 			}

	 			tools.push(obj);
	 		}

	 		// paragraph
	 		tools.push({
	 			name: 'p',
	 			text: 'P',
	 			action: _action,
	 			activeState: _activeState.bind(this, 'p'),
	 			tooltip: 'Paragraph',
	 			formatTag: 'p'
	 		});

	 		 // pre block
	 		 tools.push({
	 		 	name: 'pre',
	 		 	action: _action,
	 		 	activeState: _activeState.bind(this, 'pre'),
	 		 	tooltip: 'Preformatted Text',
	 		 	formatTag: 'pre'
	 		 });

	 		 // quote block
	 		 tools.push({
	 		 	name: 'blockquote',
	 		 	text: 'quote',
	 		 	action: _action,
	 		 	activeState: _activeState.bind(this, 'blockquote'),
	 		 	tooltip: 'Quote Selection',
	 		 	formatTag: 'blockquote'
	 		 });

	 		return tools;
	 	})(),

		[
			{
				name: 'bold',
				action: function(tool) {
					this.execCommand('bold');
				},
				activeState: function($el) {
					return queryCommandState('bold');
				},
				shortcutKey: 98
			},

			{
				name: 'underline',
				action: function(tool) {
					this.execCommand('underline')
				},
				activeState: function($el) {
					return queryCommandState('underline');
				},
				shortcutKey: 117
			},

			{
				name: 'italic',
				action: function(tool) {
					this.execCommand('italic');
				},
				activeState: function($el) {
					return queryCommandState('italic');
				},
				shortcutKey: 105
			},

			{
				name: 'strikeThrough',
				text: 'strike',
				action: function(tool) {
					this.execCommand('strikeThrough');
				},
				activeState: function($el) {
					return queryCommandState('strikeThrough');
				}
			}
		],

		// lists
		[
			{
				name: 'insertOrderedList',
				text: 'ordered list',
				action: function(tool) {
					this.execCommand('insertOrderedList');
				},
				activeState: function($el) {
					return queryCommandState('insertOrderedList');
				}
			},

			{
				name: 'insertUnorderedList',
				text: 'unordered list',
				action: function(tool) {
					this.execCommand('insertUnorderedList');
				},
				activeState: function($el) {
					return queryCommandState('insertUnorderedList');
				}
			},
		],

		// undo/redo
		[
			{
				name: 'undo',
				action: function(tool) {
					this.doUndo();
				},
				activeState: false,
				disableState: function($el) {
					return !this.canUndo();
				},
				toggleUndo: false
			},

			{
				name: 'redo',
				action: function(tool) {
					this.doUndo(true);
				},
				activeState: false,
				disableState: function($el) {
					return !this.canRedo();
				},
				toggleUndo: false
			}
		] 
	];

 	// Editor constructor.
 	return function(options) {

 		// unique editor ID
 		var id = utils.generateID(EDITOR_ID_PREFIX);

		// frequent accessed DOM objects.
		var container,			// editor container
			toolbar,			// toolbar container
			contentContainer,	// editor content window container
			editable;			// editor content editable 

		var shortcuts    = {};
		var inDesignMode = false;

		// default settings.
		var settings = {
			maxChar: 500,			// number of characters in editor
			showCharCount: true 	// should editor display the current number of characters?
		}

		var events      = new eventDispatcher();
		var undoManager = new UndoManager();

		// tools and their definitions are stored here 
		var tools = {};

		var self = this;

		// undo/redo logic variables.
		var typing      = false,
			hasChanged  = false,
			undoData    = null,
			redoData    = null,
			tmpUndoData = null,
			ctrlKeys    = [];

		var init = function() {

			// options.
			if (utils.isPlainObject(options)) {

				if (utils.has(options, 'maxChar')) {
					settings.maxChar = options.maxChar;
				}

				if (utils.has(options, 'showCharCount')) {
					settings.showCharCount = options.showCharCount;
				}

				if (utils.has(options, 'container') && options.container) {
					var _container = options.container;

					if (utils.isElement(_container)) {
						container = _container;

					} else if (typeof _container === 'string' && _container.length > 0) {
						if (DOMPurifier.sanitize('<' +_container+ '>').length > 0) {
							try {
								container = document.createElement(_container);
							} catch (error) {
								console.log('passed container parameter is an invalid string.');
								container = undefined;
							}
						}
					}
				}
			}

			// default container if not specified in options.
			if (!container) {
				container = document.createElement('DIV');
			}

			// set unique id for this container.
			container.id = id;

			// add container classes.
			container.classList.add('editor-container');
			container.classList.add('col-sm-12');	// remove?
			document.body.appendChild(container);

			setupEditorWindow();

			setupEditorEvents();

			setupHooks(events);

			// initially disable the editor
			updateEditorState(true);

		}

		// setup main editor window inside `container`.
		var setupEditorWindow = function() {

			// setup toolbar.
			toolbar = document.createElement('DIV');
			toolbar.className = 'editor-toolbar';

			// setup content editable window.
			contentContainer = document.createElement('DIV');
			contentContainer.className = 'editor-content-container';
			editable = document.createElement('DIV');
			editable.className = 'content-text';
			editable.setAttribute('contentEditable', true);

			contentContainer.appendChild(editable);
			container.appendChild(toolbar);
			container.appendChild(contentContainer);

			//@todo word count section.

			// build toolbar and tools
			renderTools(registerTools);
		}

		// setup editor main events.
		// This must be called before `setupEditorWindow`
		var setupEditorEvents = function() {

			// content editable window events.

			editable.onkeydown = function(e) {
				var keyCode = e.keyCode;

				// do shortcuts if any.
				doShortcuts.call(this, e);

				if (e.ctrlKey && keyCode !== 17) {
					ctrlKeys.push(keyCode);

					tmpUndoData = getContentWithFocus();
				}

				if ((keyCode >= 33 && keyCode <= 36) || (keyCode >= 37 && keyCode <= 40) || keyCode == 45) {
					return;
				}

				// key 91 and 224 is the windows, command key for Windows and Mac respectively.
				var modKey = (e.ctrlKey && !e.altKey) || e.metaKey || keyCode === 91 || keyCode === 224;

				if (!modKey && !typing && (keyCode < 16 || keyCode > 20)) {
					// console.log('started typing..')
					undoData = getContentWithFocus();
					typing = true;
				}
			};
			editable.onkeyup = function(e) {
				var keyCode = e.keyCode, data = getContentWithFocus();

				// keys which will update the undo:
				// 		enter (13), home (36), pgup (33), pgdn (34), end (35), insert (45),
				// 		directional keys (37-40)
				if ((keyCode == 13 || keyCode == 45 || (keyCode >= 33 && keyCode <= 40)) && typing && hasChanged) {
					// console.log('keyup: saving..')
					redoData = data;
					addCommand();

					return;
				}

				// determine if content as been changed. if user is in typing mode.
				if (typing) {
					hasChanged = undoData != null && data.content !== undoData.content;
				}

				// ctrl + keys combination was pressed.
				if (ctrlKeys.length > 0) {
					// user can also make changes if they COPY + PASTE but this wont be picked up by _hasChanged.
					// so _tmpUndoData !== content is used to bypass this.
					if (tmpUndoData.content !== data.content || hasChanged) {

						// when we dont have an _undoData we use _tmpUndoData.
						// this is because if we press ctrl+keys combination initially
						// we are not in typing mode and therefore wont have any undoData
						undoData == null && (undoData = tmpUndoData);

						redoData = data;
						addCommand();
					}

					ctrlKeys = [];
					tmpUndoData = null;
				}
			};

			editable.onmouseup = function(e) {
				// add undo level to the undo manager when back in focus.
				if (typing && undoData != null && hasChanged) {
					redoData = getContentWithFocus();
					addCommand();
				}
			};

			addEvents(editable, ['keyup', 'click'], function(e) {
				updateToolsStates();

				events.fire('toolbarStateUpdated', toolbar);
			});

			// firefox does not support `focusin` and `focusout` events.
			// instead use `focus` and `blur`.
			addEvents(editable, ['focusin', 'focusout', 'focus', 'blur'], function(e) {
				var isFocusIn = e.type === 'focusin' || e.type === 'focus' ? true : false;

				updateEditorState(!isFocusIn);
			});

		};

		// setup our main hook events.
		var setupHooks = function(dispatcher) {

			// command action.
			dispatcher.on('beforeExecuteAction', function(tool) {
				var toggleUndo = utils.has(tool, 'toggleUndo') ? tool.toggleUndo : true;

				// this command can toggle undo?
				if (toggleUndo) {

					// only set undoData if have not set yet.
					undoData == null && (undoData = getContentWithFocus());
				}

			});
			dispatcher.on('afterExecuteAction', function(tool) {
				var toggleUndo = utils.has(tool, 'toggleUndo') ? tool.toggleUndo : true;

				if (toggleUndo) {

					redoData = getContentWithFocus();

					// if content has not changed.
					if (undoData.content !== redoData.content) {
						addCommand();
					}
				}

				// update all tools states.
				updateToolsStates()

			});

			// editor designmode state.
			dispatcher.on('editorStateChanged', function(inDesignMode) {});

			// tool update
			dispatcher.on('toolStateUpdated', function(tool) {});

			dispatcher.on('toolbarStateUpdated', function($toolBar) {});

			dispatcher.on('preSetContent', function(content) {});
		};

		var getActiveTools = function() {
			var keys = [];

			for (var key in tools) {
				if (tools[key]._state) {
					keys.push(key)
				}
			}

			return keys;
		};

		var updateDisableState = function(tool) {
			var func = tool.disableState;

			if (!utils.isFunction(func)) {
				return false;
			}

			var elem = tool.elem, toDisable = func.apply(self, elem);

			return typeof toDisable !== 'undefined' && (elem.disabled = toDisable);
		};

		var updateEditorState = function(toDisable) {
			toDisable = typeof toDisable !== 'undefined' ? toDisable : false;

			inDesignMode = !toDisable;

			for (var key in tools) {
				var tool = tools[key], elem = tool.elem;

				// if tool has a `disableState` function use that instead.
				if ( !elem || updateDisableState(tool) ) {
					continue;
				}

				// update the disable status on tool elements.
				elem.disabled = toDisable;
			}

			events.fire('editorStateChanged', inDesignMode);
		};

		var updateToolState = function(tool) {

			if (!tool.activeState) {
				return false;
			}

			var elem = tool.elem;

			// register tool has `activeState` function to update the tool state.
			// we use this instead.
			if (utils.isFunction(tool.activeState)) {
				// var state = tool.activeState.apply(this, elem);
				var state = tool.activeState.apply(self, [elem]);

				typeof state !== 'undefined' && (tool._state = state);
			}

			tool._state ? elem.classList.add('active')
						: elem.classList.remove('active');

			events.fire('toolStateUpdated', tool);
		};

		// update tools active state and disable state.
		var updateToolsStates = function(keys) {
			var keys = keys || [],
				isArray = utils.isArray(keys);

			if (!isArray || (isArray && keys.length < 1)) {
				keys = utils.getKeys(tools);
			}

			for (var i = 0, tool, l = keys.length; i < l; i++) {
				tool = tools[ keys[i] ];

				updateToolState(tool);

				updateDisableState(tool);
			}
		};

		var getToolElement = function(name) {
			var id = '#' +TOOL_PREFIX+ name,
				tool = tools[name];

			// check in tools object first.
			if (tool && tool.elem) {
				return tool.elem;
			}

			var toolElem = toolbar.querySelector(id);
			return toolElem != null ? toolElem : false;
		}

		var buildToolElement = function(name) {
			var tool = tools[name],
				elemTag = tool.tag,
				toolElem;

			toolElem = getToolElement(name)

			if (toolElem || !tool) {
				return false;
			}

			if (!ALLOWED_TOOL_ELEM_TAGS.test(elemTag)) {
				return false;
			}

			toolElem = document.createElement(elemTag);
			toolElem.className = 'toolbar-tool ' + tool.class;
			toolElem.id = TOOL_PREFIX + name;
			toolElem.setAttribute('tool-name', name);
			toolElem.innerText = tool.text;

			// @todo tooltip

			// save reference to element.
			tool.elem = toolElem;

			return toolElem;
		};

		var renderTools = function(toolsDefinitions) {

			// tool groups
			for (var i = 0, l = toolsDefinitions.length; i < l; i++) {
				var len = toolsDefinitions[i].length;

				if (len < 1) {
					continue;
				}

				var group = document.createElement('DIV');
				group.className = 'btn-group';
				toolbar.appendChild(group);

				// tools
				for (var x = 0; x < len; x++) {
					var tool = toolsDefinitions[i][x], name = tool.name;

					if (!name || utils.isEmpty(name)) {
						continue;
					}

					delete tool.name;

					// could no register tool then we should skip the other process.
					if( !registerTool(name, tool)) {
						continue;
					}

					// build html for tools.
					var _el = buildToolElement(name);
					if (!_el) {
						continue;
					}

					group.appendChild(_el);

					var _tool = tools[name], toolElem = getToolElement(name);

					if (!toolElem) {
						continue;
					}

					// Using `mousedown` instead of `click` because we want this event to trigger
					// before the content editable window loses focus which will disable the tool
					//  and not allowing our tool to execute.
					toolElem.onmousedown = function(tool, e) {
						e.preventDefault();
						editable.focus();

						callToolAction(tool);

					}.bind(this, _tool);

					// shortcuts
					if (!utils.isEmpty(tool.shortcutKey)) {
						var keyCode = parseInt(tool.shortcutKey), shortcut;

						// create shortcut key using keycode as key.
						shortcut = createShortcut(keyCode, callToolAction.bind(this, tool));
					}

				}
			}

		}

		var registerTool = function(name, definitions) {
			// name needs to be string.
			if (typeof name !== 'string' || name.length < 1) {
				return false;
			}

			// definitions needs to be a plain object.
			if (!utils.isPlainObject(definitions) || utils.isFunction(definitions)) {
				return false;
			}

			var defaults = {
				tag: 'button',
				class: '',
				text: name,
				tooltip: '',
				disabled: false,
				shortcutKey: null,
				disableState: false,
				activeState: true
			};

			// override defaults with defined definitions.
			definitions = utils.extend(defaults, definitions);

			// tool private properties assigned internally.
			definitions._state = false;

			// properties that must be defined.
			if (!definitions.action || (definitions.action && !utils.isFunction(definitions.action))) {
				return false;
			}

			definitions.class += 'btn btn-default' +(!!definitions.class ? ' ' +definitions.class : '');

			return (tools[name] = definitions);
		}

		var callToolAction = function(tool) {

			// fire pre execution hook
			events.fire('beforeExecuteAction', tool);

			// toggle tool state.
			tool._state = tool._state ? false : true;

			// call tool action setting context to Editor public instance
			// and passing tool object as parameter.
			tool.action.apply(self, [tool]);

			// fire post execution hook.
			events.fire('afterExecuteAction', tool);

		};

		var createShortcut = function(pattern, callback) {
			// only add if shortcut does not exist.
			// key code will be the id of the shortcut.
			var shortcut = shortcuts[pattern] || false;
			
			if (shortcut) {
				return;
			}

			// save shortcut.
			shortcuts[pattern] = {};

			shortcut = shortcuts[pattern];
			shortcut.func = callback;

			return shortcut;
		}

		var doShortcuts = function(e) {
			// @note key code from a `keydown` event will return the key code and not the character code.
			// character codes are different for both `a` and `A`. We stick to a standard of lowercase character.
			var keyCode = e.keyCode;
			var charCode = String.fromCharCode(keyCode).toLowerCase().charCodeAt();
			var shortcut = shortcuts[charCode] || false;

			if (shortcut && e.ctrlKey) {

				// events.fire('doingShortcut', shortcut);
				(function() {
					e.preventDefault();
					shortcut.func();
				}());
			}
		}
		// get editor content.
		var getContent = function(format, inner) {

			format = format || 'html';
			inner = typeof inner !== 'undefined' ? inner : true;

			var	html = inner ? 'innerHTML' : 'outerHTML', content;

			if (format == 'text') {
				// @note `innerText` will filter out empty lines whereas `textContent` wont.
				content = editable.innerText || editable.textContent;

				// filter start and trailing spaces.
				content = utils.trim(content);

			} else if (format == 'raw') {
				return editable[html];
			} else {
				// @todo loop through innerHTML and find cases with emptiness.
				// e.g. <div></div> or <div><br /></div>
				content = editable[html];
			}

			return DOMPurifier.sanitize(content);
		}

		// set editor content.
		var setContent = function(content) {

			// pre process content.
			content = events.fire('preSetContent', content) || content;

			// sanitize html content to filter unwanted tags
			// e.g. <script></script>
			editable.innerHTML = DOMPurifier.sanitize(content);
		}

		// get content with selection data.
		// This is used for our undoManager.
		var getContentWithFocus = function() {

			var result = { content: getContent() }, _focus;

			// when user not focused on editor.
			// we temporary set focus on editor to grab the selection point.
			if (document.activeElement != editable) {
				_focus = document.activeElement;
				editable.focus();
			}

			var sel = utils.getSelection();

			if (!sel) {
				return result;
			}

			var range = sel.getRangeAt(0).cloneRange(), node;

			// @todo we want text nodes if we can. if none then just use startContainer
			// endContainer original.

			// we want only text nodes so if there are any element nodes
			// for the boundary nodes in the range we want to adjust those.
			// if (range.startContainer.nodeType != Element.TEXT_NODE) {
			// 	node = range.startContainer;
			// 	node = utils.getTextNode(node.childNodes[range.startOffset], editable, true);
			// 	range.setStart(node, node.data.length);
			// }
			// if (range.endContainer.nodeType != Element.TEXT_NODE) {
			// 	node = range.endContainer;
			// 	node = utils.getTextNode(node.childNodes[range.endOffset], editable);
			// 	range.setEnd(node, 0);
			// }

			// get the node children position index in the tree.
			// we need these to access the `startNode` in future.
			var createNodeIndexes = function(startNode, boundary) {
				var node = startNode, indexes = [], i = 0;

				while (node) {
					if (node == boundary) {
						return indexes;
					}

					if (node.previousSibling) {
						node = node.previousSibling;
						i++;
						continue;
					} else {
						indexes.push(i);
						i = 0;
					}

					node = node.parentNode;
				}

				return indexes;
			};

			var selection = {};

			selection.collapsed = range.collapsed;

			// get start and end node ref indexes.
			selection.start = {
				indexes: createNodeIndexes(range.startContainer, editable),
				offset: range.startOffset
			};

			if (!range.collapsed) {
				selection.end = {
					indexes: createNodeIndexes(range.endContainer, editable),
					offset: range.endOffset
				};
			}

			result.selection = selection;

			// update the focus back to original element if changed.
			_focus && utils.isElement(_focus) && (_focus.focus());

			return result;
		};

		// set editor content with selection state.
		// This is used with undoManager.
		var setContentWithFocus = function(data) {

			var content   = data.content,
				selection = data.selection,
				range;

			// no content? then nothing to do here.
			if (typeof content === 'undefined') {
				return;
			}

			// get node container from indexes
			var getNode = function(startNode, indexes) {
				var node = startNode, i = indexes.length -1, child;

				for ( ; i >= 0; i--) {
					child = node.childNodes[indexes[i]];

					if (!child) {
						return node;
					}

					node = child;
				}

				return node;
			};

			var setRange = function(node, offset, start) {
				start = typeof start !== 'undefined' ? start : true;

				if (node.nodeType === 1) {
					offset = Math.min(offset, node.childNodes.length);
				} else if (node.nodeType === 3) {
					offset = Math.min(offset, node.nodeValue.length);
				}

				start ? range.setStart(node, offset) : range.setEnd(node, offset);
			}

			setContent(content);

			if (selection) {
				range = document.createRange();

				// set range start point.
				var node = getNode(editable, selection.start.indexes);
				node && setRange(node, selection.start.offset, true);

				// set range end point
				if (!selection.collapsed && selection.end) {
					node = getNode(editable, selection.end.indexes);
					node && setRange(node, selection.end.offset, false);
				} else {
					node && setRange(node, selection.start.offset, false);
				}
				
				var sel = utils.getSelection();
				sel.removeAllRanges();
				sel.addRange(range);
			}

			return true;
		};

		// create a command used for undoManager
		var createCommand = function(undoData, redoData) {

			var command = {
				undoData: undoData,
				redoData: redoData,

				// `this` context is command object.
				undo: function() {
					return this.undoData;
				},

				redo: function() {
					return this.redoData;
				}
			};

			return command;
		};

		var addCommand = function() {

			var curr = undoManager.current(), command;

			// prepared datas can be same if user press ctrl key combination that
			// did not change editor content.
			// e.g. `bold` command initially wont change any content in the editor until
			// user types.
			// A fix for now is to replace the redo data in the current stack level with the `now` undo data.
			// this is not a whole solution.
			if (undoData != null && redoData != null && undoData.content === redoData.content && curr) {
				var command = createCommand(curr.undoData, undoData);
				undoManager.replace(command);
				resetUndoFlags();
				return;
			}

			command = createCommand(undoData, redoData);

			if (!command) {
				return false;
			}

			undoManager.save(command);
			
			resetUndoFlags();

			return true;
		};

		var resetUndoFlags = function() {
			typing = hasChanged = false;
			undoData = redoData = null;
		};

		// functions below are accessible to the tools register.

		// execute native command
		self.execCommand = function(cmd, showUI, value, args) {
			showUI = typeof showUI !== 'undefined' ? showUI : false;
			value  = typeof value !== 'undefined' ? value : false;

			// can only execute command in design mode.
			if (!inDesignMode) {
				return false;
			}

			// only execute commands in the native commands list.
			if (!_isNativeCommand(cmd)) {
				return false;
			}

			return document.execCommand(cmd, showUI, value);
		}

		self.getEditorElement = function() {
			return container;
		}

		self.doUndo = function(redo) {
			
			redo = typeof redo !== 'undefined' ? redo : false;

			// if user is currently typing and there must be a prepared `undoData` available.
			// ALSO if editor is currently `dirty` then add new command level.
			// This helps when there are no commands stored in the undoManager
			// but we already have a prepared undo data available.
			if (typing && undoData != null && hasChanged) {
				// console.log('doUndo: adding command')
				redoData = getContentWithFocus();
				addCommand();
			}
			
			var data = undoManager[ !redo ? 'undo' : 'redo' ]();

			resetUndoFlags();

			if (typeof data === 'undefined') {
				return false;
			}

			setContentWithFocus(data);

		}

		self.canUndo = function() {
			return undoManager.canUndo() || (undoData != null && hasChanged);
		}

		self.canRedo = function() {
			return undoManager.canRedo() && !typing;
		}

		// exposed functions.
		return {
			init : init,

			// expose some events functions.
			on : events.on,
			off : events.off,
			fire : events.fire,

			// content mutators
			getContent: getContent,
			setContent: setContent
		};

 	}

});
